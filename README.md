Welcome to the iMHealthy web application Guide
==================================================

This Java web application is deployed by AWS Elastic Beanstalk.

What's Here
-----------

This repository includes:

* README.md - this file
* .ebextensions/ - this directory contains the Java configuration file that
  allows AWS Elastic Beanstalk to deploy your Java application
* buildspec.yml - this file is used by AWS CodeBuild to build the web
  application
* pom.xml - this file is the Maven Project Object Model for the web application
* src/ - this directory contains your Java application source files


Getting Started
---------------

These directions assume you want to develop on your local computer, and not
from the Amazon EC2 instance itself. If you're on the Amazon EC2 instance, the
virtual environment is already set up for you, and you can start working on the
code.

To work on the code, you'll need to clone the
repository to your local computer. If you haven't, do that first. 

1. Install maven.  See https://maven.apache.org/install.html for details.

2. Install mysql database server

3. Install tomcat.  See https://tomcat.apache.org/tomcat-7.0-doc/setup.html for

4. Build the application.

        $ mvn -f pom.xml compile
        $ mvn -f pom.xml package

5. Copy the built application to the Tomcat webapp directory.  The actual
   location of that directory will vary depending on your platform and
   installation. (Note if you are using a different applcation containter, please look up the instructions on deploying web app)

        $ cp target/ROOT.war <tomcat webapp directory>

6. Configure system environment variables required by application.
        
        RDS_DB_NAME -> the value for this should be the database name in mysql
        RDS_USERNAME -> the user name to connect to mysql
        RDS_PASSWORD -> the password for the database
        RDS_HOSTNAME -> the hostname.
        RDS_PORT -> the mysql server port

7. Restart your tomcat server

8. Open http://127.0.0.1:8080/ in a web browser to view your application.

What Do I Do Next?
------------------

Once you have a virtual environment running, you can start making changes to
the sample Java web application.  Once you've seen how that works,
start developing your own code, and have fun!

