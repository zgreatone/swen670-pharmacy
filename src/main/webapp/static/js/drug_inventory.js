var app = angular.module('myapp', []);

app.controller('myappcontroller', function ($scope, $http) {

    $scope.drugs = [];

    getDrugs();

    loadDataTables();

    function loadDataTables() {

        $(document).ready(function () {
            $('#example').DataTable({
                "processing": true,
                "ajax": "datatable_drugs",
                "columns": [
                    {"data": "name"},
                    {
                        "data": "company.name",
                        "defaultContent": "<i>Not set</i>"
                    },
                    {
                        "data": "company.poc",
                        "defaultContent": "<i>Not set</i>"
                    },
                    {
                        "data": "company.phoneNumber",
                        "defaultContent": "<i>Not set</i>"
                    },
                    {"data": "quantity"},
                    {"data": "wholeSalePrice"},
                    {"data": "retailPrice"}
                ]
            });
        });
    }

    function getDrugs() {
        $http({
            method: 'GET',
            url: 'datatable_drugs'
        }).then(function successCallback(response) {
            $scope.drugs = response.data;
        }, function errorCallback(response) {
            console.log(response.statusText);
        });
    }

});