var app = angular.module('myapp', []);

app.controller('myappcontroller', function($scope, $http) {
    $scope.prescriptions = {}
    $scope.userform = {
        id : 0,
        homePhone : "",
        workPhone : "",
        cellPhone : "",
        street : "",
        city : "",
        state : "",
        zipCode : "",
        birthDate : ""
    };

    getPrescriptions();

    function getPrescriptions() {
        $http({
            method : 'GET',
            url : 'prescriptions'
        }).then(function successCallback(response) {
            debugger;
            $scope.prescriptions = response.data;
            debugger;
            $scope.userform.id = response.data.id;
            $scope.userform.homePhone = response.data.homePhone;
            $scope.userform.workPhone = response.data.workPhone;
            $scope.userform.cellPhone = response.data.cellPhone;
            $scope.userform.street = response.data.street;
            $scope.userform.city = response.data.city;
            $scope.userform.state = response.data.state;
            $scope.userform.zipCode = response.data.zipCode;
            $scope.userform.birthDate = response.data.birthDate;
        }, function errorCallback(response) {
            console.log(response.statusText);
        });
    }

    $scope.processUser = function()
    {
        $http({
            method : 'POST',
            url : 'user_information/update',
            data : angular.toJson($scope.userform),
            headers : {
                'Content-Type' : 'application/json'
            }
        }).then( getUserDetails(),clearForm())
            .success(function(data){
                debugger;
                $scope.user = data;
                $scope.userform.id = data.id;
                $scope.userform.homePhone = data.homePhone;
                $scope.userform.workPhone = data.workPhone;
                $scope.userform.cellPhone = data.cellPhone;
                $scope.userform.street = data.street;
                $scope.userform.city = data.city;
                $scope.userform.state = data.state;
                $scope.userform.zipCode = data.zipCode;
                $scope.userform.birthDate = data.birthDate;
            });
    };

    function clearForm() {
        $scope.userform.id = 0;
        $scope.userform.homePhone = "";
        $scope.userform.workPhone = "";
        $scope.userform.cellPhone = "";
        $scope.userform.street = "";
        $scope.userform.city = "";
        $scope.userform.state = "";
        $scope.userform.zipCode = "";
        $scope.userform.birthDate = "";
    }

    function disableName()
    {
        document.getElementById("name").disabled = true;
    }
});