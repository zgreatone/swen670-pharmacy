var app = angular.module('myapp', []);

app.controller('myappcontroller', function($scope, $http) {
    $scope.surveyform = {
        id : 0,
        question1 : "NO",
        question2 : "NO",
        question3 : "NO",
        question4 : "NO",
        question5 : "NO",
        question6 : "NO",
        question7 : "NO",
        question8 : "NO",
        question9 : "NO",
        question10 : ""
    };

    getSurvey();

    function getSurvey() {
        $http({
            method : 'GET',
            url : 'surveys'
        }).then(function successCallback(response) {
            $scope.surveyform = response.data;
        }, function errorCallback(response) {
            console.log(response.statusText);
        });
    }

    $scope.processSurvey = function()
    {
        $http({
            method : 'POST',
            url : 'update_survey',
            data : angular.toJson($scope.surveyform),
            headers : {
                'Content-Type' : 'application/json'
            }
        }).then( clearForm())
            .success(function(data){
                debugger;
                $scope.surveyform = data;
            });
    };

    function clearForm() {
        $scope.surveyform.id = 0;
        $scope.surveyform.question1 = "NO";
        $scope.surveyform.question2 = "NO";
        $scope.surveyform.question3 = "NO";
        $scope.surveyform.question4 = "NO";
        $scope.surveyform.question5 = "NO";
        $scope.surveyform.question6 = "NO";
        $scope.surveyform.question7 = "NO";
        $scope.surveyform.question8 = "NO";
        $scope.surveyform.question9 = "NO";
        $scope.surveyform.question10 = "";
    }

    function disableName()
    {
        document.getElementById("name").disabled = true;
    }
});