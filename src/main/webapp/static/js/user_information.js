var app = angular.module('myapp', []);

app.controller('myappcontroller', function ($scope, $http) {
    $scope.user = {};
    $scope.insuranceList = [];
    $scope.userform = {
        id: 0,
        homePhone: "",
        workPhone: "",
        cellPhone: "",
        street: "",
        city: "",
        state: "",
        zipCode: "",
        birthDate: "",
        user: {
            firstName: "",
            middleName: "",
            lastName: ""
        }
    };
    $scope.insuranceform = {
        id: 0,
        companyName: "",
        coPay: 0.0,
        primaryHolderName: "",
        insurancePhoneNumber: "",
        insuranceGroupNumber: "",
        billingStreet: "",
        billingState: "",
        billingCity: "",
        billingZip: ""
    };

    getUserDetails();

    getUserInsurance();

    function getUserDetails() {
        $http({
            method: 'GET',
            url: 'user_information/userdetails'
        }).then(function successCallback(response) {
            $scope.user = response.data;
            debugger;
            $scope.userform.id = response.data.id;
            $scope.userform.homePhone = response.data.homePhone;
            $scope.userform.workPhone = response.data.workPhone;
            $scope.userform.cellPhone = response.data.cellPhone;
            $scope.userform.street = response.data.street;
            $scope.userform.city = response.data.city;
            $scope.userform.state = response.data.state;
            $scope.userform.zipCode = response.data.zipCode;
            $scope.userform.birthDate = response.data.birthDate;
            $scope.userform.user = response.data.user;
        }, function errorCallback(response) {
            console.log(response.statusText);
        });
    }

    function getUserInsurance() {
        $http({
            method: 'GET',
            url: 'user_information/insurance'
        }).then(function successCallback(response) {
            $scope.insuranceList = response.data;
        }, function errorCallback(response) {
            console.log(response.statusText);
        });
    }

    $scope.processUser = function () {
        debugger;
        var  test = angular.toJson($scope.userform)
        $http({
            method: 'POST',
            url: 'user_information/update',
            data: angular.toJson($scope.userform),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(getUserInsurance(), clearForm())
            .success(function (data) {
                $scope.user = data;
                $scope.userform.id = data.id;
                $scope.userform.homePhone = data.homePhone;
                $scope.userform.workPhone = data.workPhone;
                $scope.userform.cellPhone = data.cellPhone;
                $scope.userform.street = data.street;
                $scope.userform.city = data.city;
                $scope.userform.state = data.state;
                $scope.userform.zipCode = data.zipCode;
                $scope.userform.birthDate = data.birthDate;
                $scope.userform.user = data.user;
            });
    };

    $scope.processInsurance = function () {
        $http({
            method: 'POST',
            url: 'user_information/update_insurance',
            data: angular.toJson($scope.insuranceform),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(getUserDetails(), clearInsuranceForm())
            .success(function (data) {
                location.reload(true);
            });
    };

    function clearForm() {
        $scope.userform.id = 0;
        $scope.userform.homePhone = "";
        $scope.userform.workPhone = "";
        $scope.userform.cellPhone = "";
        $scope.userform.street = "";
        $scope.userform.city = "";
        $scope.userform.state = "";
        $scope.userform.zipCode = "";
        $scope.userform.birthDate = "";
        $scope.userform.user = {
            firstName: "",
            middleName: "",
            lastName: ""
        }
    }

    function clearInsuranceForm() {
        $scope.userform.id = 0;
        $scope.userform.homePhone = "";
        $scope.userform.workPhone = "";
        $scope.userform.cellPhone = "";
        $scope.userform.street = "";
        $scope.userform.city = "";
        $scope.userform.state = "";
        $scope.userform.zipCode = "";
        $scope.userform.birthDate = "";
        $scope.userform.user = {
            firstName: "",
            middleName: "",
            lastName: ""
        }
    }

    function disableName() {
        document.getElementById("name").disabled = true;
    }
});

