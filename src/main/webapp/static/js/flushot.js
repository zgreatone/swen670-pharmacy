var app = angular.module('myapp', []);

app.controller('myappcontroller', function($scope, $http) {
    $scope.fluShots = [];
    $scope.myFluShots = [];
    $scope.flushotform = {
        id : 0,
        fluShot : "",
        dateScheduled : "",
        dateAdministered : ""
    };

    getFlushots();

    function getFlushots() {
        $http({
            method : 'GET',
            url : 'fluShots'
        }).then(function successCallback(response) {
            debugger;
            $scope.fluShots = response.data.fluShots;
            $scope.myFluShots = response.data.myFluShots;
            $scope.flushotform = response.data.latest;
            debugger;
        }, function errorCallback(response) {
            console.log(response.statusText);
        });
    }

    $scope.processFlushot = function()
    {
        $http({
            method : 'POST',
            url : 'update_flushot',
            data : angular.toJson($scope.flushotform),
            headers : {
                'Content-Type' : 'application/json'
            }
        }).then( clearForm())
            .success(function(data){
                location.reload();
            });
    };

    function clearForm() {
        $scope.flushotform.id = 0;
        $scope.flushotform.fluShot = "";
        $scope.flushotform.dateScheduled = "";
        $scope.flushotform.dateAdministered = "";
    }

    function disableName()
    {
        document.getElementById("name").disabled = true;
    }
});