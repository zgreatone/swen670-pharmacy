var app = angular.module('myapp', []);

app.controller('myappcontroller', function ($scope, $http) {

    $scope.patients = [];

    getPatients();

    loadDataTables();

    function loadDataTables() {

        $(document).ready(function () {
            $('#example').DataTable({
                "processing": true,
                "ajax": "patients",
                "columns": [
                    {"data": "firstName"},
                    {"data": "lastName"},
                    {"data": "email"},
                    {
                        "data": "userInformation.birthDate",
                        "defaultContent": "<i>Not set</i>"
                    },
                    {
                        "data": "userInformation.state",
                        "defaultContent": "<i>Not set</i>"
                    },
                    {
                        "data": "userInformation.zipCode",
                        "defaultContent": "<i>Not set</i>"
                    }
                ]
            });
        });
    }

    function getPatients() {
        $http({
            method: 'GET',
            url: 'patients'
        }).then(function successCallback(response) {
            $scope.patients = response.data;
        }, function errorCallback(response) {
            console.log(response.statusText);
        });
    }

});