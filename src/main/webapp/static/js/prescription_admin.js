var app = angular.module('myapp', []);

app.controller('myappcontroller', function ($scope, $http) {
    $scope.users = [];
    $scope.drugs = [];
    $scope.userform = {
        id: 0,
        patient: "",
        drug: "",
        doctorName: "",
        doctorNumber: "",
        dosage: "",
        refillCount: "",
        startDate: ""
    };

    getUsers();
    getDrugs();

    loadDataTables();

    function loadDataTables() {

        $(document).ready(function () {
            $('#example').DataTable({
                "processing": true,
                "ajax": "datatable_prescriptions",
                "columns": [
                    {"data": "user.firstName"},
                    {"data": "user.lastName"},
                    {
                        "data": "drug.name",
                        "defaultContent": "<i>Not set</i>"
                    },
                    {
                        "data": "doctorName",
                        "defaultContent": "<i>Not set</i>"
                    },
                    {"data": "doctorPhoneNumber"},
                    {"data": "dosage"},
                    {"data": "refillCount"}
                ],
                "columnDefs": [
                    {
                        // The `data` parameter refers to the data for the cell (defined by the
                        // `data` option, which defaults to the column being worked with, in
                        // this case `data: 0`.
                        "render": function (data, type, row) {
                            return data + ' ' + row["user"].lastName + '';
                        },
                        "targets": 0
                    },
                    {"visible": false, "targets": [1]}
                ]
            });
        });
    }

    function getUsers() {
        $http({
            method: 'GET',
            url: 'users'
        }).then(function successCallback(response) {
            debugger;
            $scope.users = response.data;
        }, function errorCallback(response) {
            console.log(response.statusText);
        });
    }

    function getDrugs() {
        $http({
            method: 'GET',
            url: 'drugs'
        }).then(function successCallback(response) {
            debugger;
            $scope.drugs = response.data;
        }, function errorCallback(response) {
            console.log(response.statusText);
        });
    }

    $scope.processPrescription = function () {
        $http({
            method: 'POST',
            url: 'create',
            data: angular.toJson($scope.userform),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(getUsers(), getDrugs(), clearForm())
            .success(function (data) {
                debugger;
                location.reload(true);

            });
    };

    function clearForm() {
        $scope.userform.id = 0;
        $scope.userform.patient = "";
        $scope.userform.drug = "";
        $scope.userform.doctorName = "";
        $scope.userform.doctorNumber = "";
        $scope.userform.startDate = "";
        $scope.userform.dosage = "";
        $scope.userform.refillCount = "";
    }

    function disableName() {
        document.getElementById("name").disabled = true;
    }
});