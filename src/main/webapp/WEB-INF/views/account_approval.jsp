<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/static/favicon.ico">

    <title>iMHealthy - Home</title>

    <!-- Bootstrap core CSS -->
    <link href="/static/css/bootstrap.min.css" rel="stylesheet">

    <!-- Font Awesome CSS -->
    <link href="/static/css/font-awesome.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="/static/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/static/css/starter-template.css" rel="stylesheet">



    <!-- DatePicker -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker3.css">
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]>
    <script src="/static/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="/static/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>

    <script type="text/javascript">
        var app = angular.module('myapp', []);

        app.controller('myappcontroller', function($scope, $http) {
            $scope.users = []

            getUsers();

            function getUsers() {
                $http({
                    method : 'GET',
                    url : 'users'
                }).then(function successCallback(response) {
                    debugger;
                    $scope.users = response.data;
                }, function errorCallback(response) {
                    console.log(response.statusText);
                });
            }

            $scope.activateUser = function(user) {
                $http({
                    method : 'POST',
                    url : 'activate_user',
                    data : angular.toJson(user),
                    headers : {
                        'Content-Type' : 'application/json'
                    }
                }).then( getUsers());
            };

            $scope.deleteUser = function(user) {
                $http({
                    method : 'DELETE',
                    url : 'delete_user',
                    data : angular.toJson(user),
                    headers : {
                        'Content-Type' : 'application/json'
                    }
                }).then( getUsers());
            };


        });



    </script>
</head>

<body ng-app="myapp" ng-controller="myappcontroller">

<%@include file="navbar_include.jsp" %>

<div class="container">

    <h3>Pharmacist Accounts To Be Activated</h3>
    <div class="table-responsive">
        <table class="table table-bordered" style="width: 600px">
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Actions</th>
            </tr>

            <tr ng-repeat="user in users">
                <td>{{ user.firstName}}</td>
                <td>{{ user.lastName }}</td>
                <td>{{ user.email}}</td>
                <td><a ng-click="activateUser(user)" class="btn btn-primary btn-sm">Activate</a>
                    | <a ng-click="deleteUser(user)" class="btn btn-danger btn-sm">Delete</a></td>
            </tr>
        </table>
    </div>

</div><!-- /.container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/static/assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="/static/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="/static/assets/js/ie10-viewport-bug-workaround.js"></script>
<script>
//    $('#sandbox-container .input-group.date').datepicker({
//        autoclose: true,
//        format: "yyyy-mm-dd"
//    });
//
//    $('.datepicker').datepicker({
//        autoclose: true,
//        format: "yyyy-mm-dd"
//    });
$('#mydatepicker').datepicker({
    autoclose: true,
    format: "yyyy-mm-dd"
});
$('#mydatepicker').on('changeDate', function() {
    $('#birthdate').val(
        $('#mydatepicker').datepicker('getFormattedDate')
    );
    var myinput = $("#birthdate");
    angular.element(myinput).triggerHandler("input");
});
</script>
</body>
</html>
