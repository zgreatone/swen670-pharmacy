<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/static/favicon.ico">

    <title>iMHealthy - Medical Survey</title>

    <!-- Bootstrap core CSS -->
    <link href="/static/css/bootstrap.min.css" rel="stylesheet">

    <!-- Font Awesome CSS -->
    <link href="/static/css/font-awesome.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="/static/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/static/css/starter-template.css" rel="stylesheet">



    <!-- DatePicker -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker3.css">
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]>
    <script src="/static/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="/static/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>

    <script src="/static/js/survey.js"></script>

</head>

<body ng-app="myapp" ng-controller="myappcontroller">

<%@include file="navbar_include.jsp" %>

<div class="container">

    <div class="row">

        <div class="col-sm-8">
            <h3><spring:message code="page.medicalhistory"> </spring:message></h3>
            <form name="myForm" >
                <div class="input-group input-group-sm">
                    <input type="checkbox" ng-model="surveyform.question1" id="Question1" ng-true-value="'YES'" ng-false-value="'NO'" />
                    <label class="checkbox-inline" for="Question1"><spring:message code="page.highbloodpressure"> </spring:message></label>
                </div>
                <br/>
                <div class="input-group input-group-sm">
                    <input type="checkbox" ng-model="surveyform.question2" id="Question2" ng-true-value="'YES'" ng-false-value="'NO'" />
                    <label class="checkbox-inline" for="Question2"><spring:message code="page.heartdisease"> </spring:message></label>
                </div>
                <br/>
                <div class="input-group input-group-sm">
                    <input type="checkbox" ng-model="surveyform.question3" id="Question3" ng-true-value="'YES'" ng-false-value="'NO'" />
                    <label class="checkbox-inline"for="Question2"><spring:message code="page.adema"> </spring:message></label>
                </div>
                <br/>
                <div class="input-group input-group-sm">
                    <input type="checkbox" ng-model="surveyform.question4" id="Question4" ng-true-value="'YES'" ng-false-value="'NO'" />
                    <label class="checkbox-inline"for="Question2"><spring:message code="page.hypothorodism"> </spring:message></label>
                </div>
                <br/>
                <div class="input-group input-group-sm">
                    <input type="checkbox" ng-model="surveyform.question5" id="Question5" ng-true-value="'YES'" ng-false-value="'NO'" />
                    <label class="checkbox-inline"for="Question2"><spring:message code="page.diabetes"> </spring:message></label>
                </div>
                <br/>
                <div class="input-group input-group-sm">
                    <input type="checkbox" ng-model="surveyform.question6" id="Question6" ng-true-value="'YES'" ng-false-value="'NO'" />
                    <label class="checkbox-inline"for="Question2"><spring:message code="page.reslesslegsyndrome"> </spring:message></label>
                </div>
                <br/>
                <div class="input-group input-group-sm">
                    <input type="checkbox" ng-model="surveyform.question7" id="Question7" ng-true-value="'YES'" ng-false-value="'NO'" />
                    <label class="checkbox-inline"for="Question2"><spring:message code="page.asthma"> </spring:message></label>
                </div>
                <br/>
                <div class="input-group input-group-sm">
                    <input type="checkbox" ng-model="surveyform.question8" id="Question8" ng-true-value="'YES'" ng-false-value="'NO'" />
                    <label class="checkbox-inline"for="Question2"><spring:message code="page.emphysema"> </spring:message></label>
                </div>
                <br/>
                <div class="input-group input-group-sm">
                    <input type="checkbox" ng-model="surveyform.question9" id="Question9" ng-true-value="'YES'" ng-false-value="'NO'" />
                    <label class="checkbox-inline"for="Question2"><spring:message code="page.migrane_orrecurrentheadaches"> </spring:message></label>
                </div>
                <br/>

                <label>
                    <spring:message code="page.allergies"> </spring:message>:
                    <textarea class="form-control" rows="5" ng-model="surveyform.question10"></textarea>
                </label><br/>
                <input type="submit"
                       class="btn btn-primary btn-sm" ng-click="processSurvey()"
                       value="Save"/>
            </form>
        </div>



    </div>

</div><!-- /.container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/static/assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="/static/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="/static/assets/js/ie10-viewport-bug-workaround.js"></script>
<script>
    //    $('#sandbox-container .input-group.date').datepicker({
    //        autoclose: true,
    //        format: "yyyy-mm-dd"
    //    });
    //
    //    $('.datepicker').datepicker({
    //        autoclose: true,
    //        format: "yyyy-mm-dd"
    //    });
    $('#mydatepicker').datepicker({
        autoclose: true,
        format: "yyyy-mm-dd"
    });
    $('#mydatepicker').on('changeDate', function() {
        $('#birthdate').val(
            $('#mydatepicker').datepicker('getFormattedDate')
        );
        var myinput = $("#birthdate");
        angular.element(myinput).triggerHandler("input");
    });
</script>
</body>
</html>
