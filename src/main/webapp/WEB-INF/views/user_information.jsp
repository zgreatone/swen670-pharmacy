<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/static/favicon.ico">

    <title>iMHealthy - User Information</title>

    <!-- Bootstrap core CSS -->
    <link href="/static/css/bootstrap.min.css" rel="stylesheet">

    <!-- Font Awesome CSS -->
    <link href="/static/css/font-awesome.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="/static/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/static/css/starter-template.css" rel="stylesheet">


    <!-- DatePicker -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker3.css">
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]>
    <script src="/static/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="/static/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>

    <script src="/static/js/user_information.js"></script>
</head>

<body ng-app="myapp" ng-controller="myappcontroller">

<%@include file="navbar_include.jsp" %>

<div class="container">

    <div class="row">

        <div class="col-sm-8">
            <h3><spring:message code="page.userinformationupdate"></spring:message></h3>
            <form ng-submit="processUserDetails()">
                <div class="table-responsive">
                    <table class="table table-bordered" style="width: 450px">
                        <tr>
                            <td><spring:message code="page.firstname"></spring:message> </td>
                            <td><input type="text" id="firstName" ng-model="userform.user.firstName" size="30"/></td>
                        </tr>
                        <tr>
                            <td><spring:message code="page.middlename"></spring:message> </td>
                            <td><input type="text" id="middleName" ng-model="userform.user.middleName" size="30"/></td>
                        </tr>
                        <tr>
                            <td><spring:message code="page.lastname"></spring:message> </td>
                            <td><input type="text" id="lastName" ng-model="userform.user.lastName" size="30"/></td>
                        </tr>
                        <tr>
                            <td><spring:message code="page.homephone"></spring:message></td>
                            <td><input type="text" id="homePhone" ng-model="userform.homePhone" size="30"/></td>
                        </tr>
                        <tr>
                            <td><spring:message code="page.cellhphone"></spring:message></td>
                            <td><input type="text" id="cellPhone" ng-model="userform.cellPhone"
                                       size="30"/></td>
                        </tr>
                        <tr>
                            <td><spring:message code="page.workphone"></spring:message></td>
                            <td><input type="text" id="workPhone" ng-model="userform.workPhone" size="30"/></td>
                        </tr>
                        <tr>
                            <td><spring:message code="page.birthdate"></spring:message></td>
                            <td>
                                <div id="mydatepicker" class="input-group date">
                                    <input id="birthdate" type="text" class="form-control" ng-model="userform.birthDate"
                                           size="30">
                                    <div class="input-group-addon">
                                        <span class="fa fa-calendar"></span>
                                    </div>

                                </div>

                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="page.Street"></spring:message></td>
                            <td><input type="text" id="street" ng-model="userform.street" size="30"/></td>
                        </tr>
                        <tr>
                            <td><spring:message code="page.city"></spring:message></td>
                            <td><input type="text" id="city" ng-model="userform.city" size="30"/></td>
                        </tr>
                        <tr>
                            <td><spring:message code="page.state"></spring:message></td>
                            <td><input type="text" id="state" ng-model="userform.state"
                                       size="30"/></td>
                        </tr>

                        <tr>
                            <td><spring:message code="page.zipcode"></spring:message></td>
                            <td><input type="text" id="zipCode" ng-model="userform.zipCode"
                                       size="30"/></td>
                        </tr>
                        <tr>
                            <td colspan="2"><input type="submit"
                                                   class="btn btn-primary btn-sm" ng-click="processUser()"
                                                   value="<spring:message code="page.Updateuserinformation"></spring:message>"/></td>
                        </tr>
                    </table>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-5">
            <h3><spring:message code="page.insurance"></spring:message></h3>
            <form ng-submit="processUserDetails()">
                <div class="table-responsive">
                    <table class="table table-bordered" style="width: 400px">
                        <tr>
                            <td><spring:message code="page.companyname"></spring:message> </td>
                            <td><input type="text" id="companyName" ng-model="insuranceform.companyName" size="30"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="page.copay"></spring:message> </td>
                            <td><input type="number" step=".01" id="coPay" ng-model="insuranceform.coPay" size="30"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="page.primaryholdername"></spring:message>  </td>
                            <td><input type="text" id="primaryHolderName" ng-model="insuranceform.primaryHolderName"
                                       size="30"/></td>
                        </tr>
                        <tr>
                            <td><spring:message code="page.insurancephonenumber"></spring:message>  </td>
                            <td><input type="text" id="insurancePhoneNumber"
                                       ng-model="insuranceform.insurancePhoneNumber" size="30"/></td>
                        </tr>
                        <tr>
                            <td><spring:message code="page.groupnumber"></spring:message> </td>
                            <td><input type="text" id="insuranceGroupNumber"
                                       ng-model="insuranceform.insuranceGroupNumber" size="30"/></td>
                        </tr>
                        <tr>
                            <td><spring:message code="page.billingstreet"></spring:message> </td>
                            <td><input type="text" id="billingStreet" ng-model="insuranceform.billingStreet" size="30"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="page.billingstate"></spring:message> </td>
                            <td><input type="text" id="billingState" ng-model="insuranceform.billingState" size="30"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="page.billingcity"></spring:message> </td>
                            <td><input type="text" id="billingCity" ng-model="insuranceform.billingCity" size="30"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="page.billingzip"></spring:message> </td>
                            <td><input type="text" id="billingZip" ng-model="insuranceform.billingZip" size="30"/></td>
                        </tr>
                        <tr>
                            <td colspan="2"><input type="submit"
                                                   class="btn btn-primary btn-sm" ng-click="processInsurance()"
                                                   value="<spring:message code="page.updateinsuranceinformation"></spring:message>"/></td>
                        </tr>
                    </table>
                </div>
            </form>
        </div>
        <div class="col-sm-7">

            <h3><spring:message code="page.insurancelist"></spring:message> </h3>
            <div class="table-responsive">
                <table class="table table-bordered" style="width: 500px">
                    <tr>
                        <th><spring:message code="page.companyname"></spring:message> </th>
                        <th><spring:message code="page.policyholdername"></spring:message>  </th>
                        <th><spring:message code="page.groupnumber"></spring:message> </th>
                        <th><spring:message code="page.phonenumber"></spring:message> </th>
                        <th><spring:message code="page.actions"></spring:message></th>
                    </tr>

                    <tr ng-repeat="insurance in insuranceList">
                        <td>{{ insurance.companyName}}</td>
                        <td>{{ insurance.primaryHolderName }}</td>
                        <td>{{ insurance.insuranceGroupNumber }}</td>
                        <td>{{ insurance.insurancePhoneNumber}}</td>
                        <td><a ng-click="editInsurance(insurance)" class="btn btn-primary btn-sm">Edit</a>
                            |<a ng-click="deleteInsurance(insurance)" class="btn btn-primary btn-sm">Delete</a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>


    </div>


</div><!-- /.container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/static/assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="/static/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="/static/assets/js/ie10-viewport-bug-workaround.js"></script>
<script>
    //    $('#sandbox-container .input-group.date').datepicker({
    //        autoclose: true,
    //        format: "yyyy-mm-dd"
    //    });
    //
    //    $('.datepicker').datepicker({
    //        autoclose: true,
    //        format: "yyyy-mm-dd"
    //    });
    $('#mydatepicker').datepicker({
        autoclose: true,
        format: "yyyy-mm-dd"
    });
    $('#mydatepicker').on('changeDate', function () {
        $('#birthdate').val(
            $('#mydatepicker').datepicker('getFormattedDate')
        );
        var myinput = $("#birthdate");
        angular.element(myinput).triggerHandler("input");
    });
</script>
</body>
</html>
