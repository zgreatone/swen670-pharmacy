<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <sec:authorize access="isAnonymous()">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target=" <c:url value='/' > <c:param name="lang" value="${pageContext.response.locale}" /> </c:url>"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand"
                   href=" <c:url value='/' > <c:param name="lang" value="${pageContext.response.locale}" /> </c:url>">iMHealthy</a>
            </sec:authorize>
            <sec:authorize access="isAuthenticated()">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target=" <c:url value='/welcome' > <c:param name="lang" value="${pageContext.response.locale}" /> </c:url>"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand"
                   href=" <c:url value='/welcome' > <c:param name="lang" value="${pageContext.response.locale}" /> </c:url>">iMHealthy</a>
            </sec:authorize>

        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <sec:authorize access="isAnonymous()">
                    <li class="${pageContext.request.requestURI eq '/WEB-INF/views/welcome.jsp' ? ' active' : ''}"><a
                            href=" <c:url value='/' > <c:param name="lang" value="${pageContext.response.locale}" /> </c:url>"><spring:message
                            code="nav.home"/></a></li>
                </sec:authorize>
                <sec:authorize access="isAuthenticated()">
                    <li class="${pageContext.request.requestURI eq '/WEB-INF/views/welcome.jsp' ? ' active' : ''}"><a
                            href=" <c:url value='/welcome' > <c:param name="lang" value="${pageContext.response.locale}" /> </c:url>"><spring:message
                            code="nav.home"/></a></li>
                </sec:authorize>
                <sec:authorize access="hasRole('PATIENT')">
                    <li class="${pageContext.request.requestURI eq '/WEB-INF/views/user_information.jsp' ? ' active' : ''}">
                        <a href=" <c:url value='/user_information' > <c:param name="lang" value="${pageContext.response.locale}" /> </c:url>"><spring:message
                                code="nav.info"/></a></li>
                    <li class="${pageContext.request.requestURI eq '/WEB-INF/views/prescription.jsp' ? ' active' : ''}">
                        <a href=" <c:url value='/patient/prescription' > <c:param name="lang" value="${pageContext.response.locale}" /> </c:url>">
                            <spring:message code="nav.prescription"/>
                        </a>
                    </li>
                    <li class="${pageContext.request.requestURI eq '/WEB-INF/views/survey.jsp' ? ' active' : ''}">
                        <a href=" <c:url value='/patient/survey' > <c:param name="lang" value="${pageContext.response.locale}" /> </c:url>">
                            <spring:message code="nav.survey"/>
                        </a>
                    </li>
                    <li class="${pageContext.request.requestURI eq '/WEB-INF/views/flushot.jsp' ? ' active' : ''}">
                        <a href=" <c:url value='/patient/flushot' > <c:param name="lang" value="${pageContext.response.locale}" /> </c:url>">
                            <spring:message code="nav.flushot"/>
                        </a>
                    </li>
                    <%--<li><a href="#phamacist">Pharmacist</a></li>--%>
                </sec:authorize>
                <sec:authorize access="hasRole('PHARMACIST')">
                    <li class="${pageContext.request.requestURI eq '/WEB-INF/views/prescription_admin.jsp' ? ' active' : ''}">
                        <a href=" <c:url value='/pharmacist/prescription' > <c:param name="lang" value="${pageContext.response.locale}" /> </c:url>">
                            <spring:message code="nav.prescription"/>
                        </a>
                    </li>
                    <li class="${pageContext.request.requestURI eq '/WEB-INF/views/drug_inventory.jsp' ? ' active' : ''}">
                        <a href=" <c:url value='/pharmacist/drug_inventory' > <c:param name="lang" value="${pageContext.response.locale}" /> </c:url>">
                            <spring:message code="nav.inventory"/>
                        </a>
                    </li>
                    <li class="${pageContext.request.requestURI eq '/WEB-INF/views/patient_record.jsp' ? ' active' : ''}">
                        <a href=" <c:url value='/pharmacist/patient_record' > <c:param name="lang" value="${pageContext.response.locale}" /> </c:url>">
                            <spring:message code="nav.patient_record"/>
                        </a>
                    </li>
                    <li class="${pageContext.request.requestURI eq '/WEB-INF/views/billing.jsp' ? ' active' : ''}">
                        <a href=" <c:url value='/pharmacist/billing' > <c:param name="lang" value="${pageContext.response.locale}" /> </c:url>">
                            <spring:message code="nav.billing"/>
                        </a>
                    </li>
                    <li class="${pageContext.request.requestURI eq '/WEB-INF/views/drug_compounding.jsp' ? ' active' : ''}">
                        <a href=" <c:url value='/pharmacist/drug_compounding' > <c:param name="lang" value="${pageContext.response.locale}" /> </c:url>">
                            <spring:message code="nav.drug_compounding"/>
                        </a>
                    </li>
                    <%--<li><a href="#patient">Patient</a></li>--%>
                </sec:authorize>
                <sec:authorize access="hasRole('ADMIN')">
                    <li class="${pageContext.request.requestURI eq '/WEB-INF/views/account_approval.jsp' ? ' active' : ''}">
                        <a href=" <c:url value='/admin/approve' > <c:param name="lang" value="${pageContext.response.locale}" /> </c:url>"><spring:message
                                code="nav.account_approval"/></a></li>
                    <%--<li><a href="#patient">Patient</a></li>--%>
                </sec:authorize>
                <%--<li class="${pageContext.request.requestURI eq '/WEB-INF/views/about.jsp' ? ' active' : ''}">--%>
                <%--<a href="#about"><spring:message code="nav.about"/></a></li>--%>
                <%--<li class="${pageContext.request.requestURI eq '/WEB-INF/views/contact.jsp' ? ' active' : ''}">--%>
                <%--<a href="#contact"><spring:message code="nav.contact"/></a></li>--%>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <sec:authorize access="isAnonymous()">
                    <li class="${pageContext.request.requestURI eq '/WEB-INF/views/login.jsp' ? ' active' : ''}">
                        <a
                                href=" <c:url value='/login' > <c:param name="lang" value="${pageContext.response.locale}" /> </c:url>">
                            <i style="padding-right: 5px" class="fa fa-sign-in"></i>
                            <spring:message code="form.login"/>
                        </a>
                    </li>
                    <li class="${pageContext.request.requestURI eq '/WEB-INF/views/registration.jsp' ? ' active' : ''}">
                        <a href=" <c:url value='/registration' > <c:param name="lang" value="${pageContext.response.locale}" /> </c:url>">
                            <i style="padding-right: 5px" class="fa fa-camera"></i>
                            <spring:message code="nav.register"/>
                        </a>
                    </li>
                </sec:authorize>

                <%--<sec:authorize access="hasRole('PHARMACIST')">--%>
                <%--<li class="dropdown">--%>
                <%--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"--%>
                <%--aria-expanded="false">Pharmacist<span class="caret"></span></a>--%>
                <%--<ul class="dropdown-menu">--%>
                <%--<li><a href="#">Action</a></li>--%>
                <%--<li><a href="#">Another action</a></li>--%>
                <%--<li><a href="#">Something else here</a></li>--%>
                <%--<li role="separator" class="divider"></li>--%>
                <%--<li><a href="#">Separated link</a></li>--%>
                <%--</ul>--%>
                <%--</li>--%>
                <%--</sec:authorize>--%>
                <%--<sec:authorize access="hasRole('PATIENT')">--%>
                <%--<li class="dropdown">--%>
                <%--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"--%>
                <%--aria-expanded="false">Patient<span class="caret"></span></a>--%>
                <%--<ul class="dropdown-menu">--%>
                <%--<li><a href="#">Action</a></li>--%>
                <%--<li><a href="#">Another action</a></li>--%>
                <%--<li><a href="#">Something else here</a></li>--%>
                <%--<li role="separator" class="divider"></li>--%>
                <%--<li><a href="#">Separated link</a></li>--%>
                <%--</ul>--%>
                <%--</li>--%>
                <%--</sec:authorize>--%>
                <%--<sec:authorize access="hasRole('ADMIN')">--%>
                <%--<li class="dropdown">--%>
                <%--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"--%>
                <%--aria-expanded="false">Administration<span class="caret"></span></a>--%>
                <%--<ul class="dropdown-menu">--%>
                <%--<li><a href="#">Approve Accounts</a></li>--%>
                <%--<li role="separator" class="divider"></li>--%>
                <%--<li><a href="#">Separated link</a></li>--%>
                <%--</ul>--%>
                <%--</li>--%>
                <%--</sec:authorize>--%>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false"><spring:message code="nav.language"/><span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="?lang=en">English</a>
                        </li>
                        <li>
                            <a href="?lang=es">Espanol</a>
                        </li>
                    </ul>
                </li>
                <sec:authorize access="isAuthenticated()">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle profile-image" data-toggle="dropdown">
                            <img src="http://placehold.it/30x30" class="img-circle special-img"> ${loggedinuser} <b
                                class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="#"><i style="padding-right: 5px" class="fa fa-cog"></i>Account</a></li>
                            <li class="divider"></li>
                            <li><a href="<c:url value='/logout' />"><i style="padding-right: 5px"
                                                                       class="fa fa-sign-out"></i>Logout</a></li>
                        </ul>
                    </li>
                </sec:authorize>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>