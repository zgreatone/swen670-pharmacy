package edu.umuc.swen670.fall2017.pharmacy.controller;

import edu.umuc.swen670.fall2017.pharmacy.model.Insurance;
import edu.umuc.swen670.fall2017.pharmacy.model.User;
import edu.umuc.swen670.fall2017.pharmacy.model.UserInformation;
import edu.umuc.swen670.fall2017.pharmacy.service.PatientPortalService;
import edu.umuc.swen670.fall2017.pharmacy.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping("/user_information")
public class UserInformationController {

    @Autowired
    private UserService userService;

    @Autowired
    private PatientPortalService patientPortalService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String getPage(HttpServletRequest request, HttpServletResponse response, Model model) {

        model.addAttribute("user", getCurrentLoggedInUser());
        return "user_information";
    }

    @RequestMapping(value = {"/userdetails"})
    public ResponseEntity<UserInformation> getUserDetails(HttpServletRequest request, HttpServletResponse response) {

        final User currentUser = getCurrentLoggedInUser();

        UserInformation userInformation = currentUser.getUserInformation();

        if (userInformation == null) {
            userInformation = new UserInformation();
        }

        currentUser.setUserInformation(null);
        userInformation.setUser(currentUser);


        return new ResponseEntity<>(userInformation, HttpStatus.OK);
    }

    @RequestMapping(value = {"/insurance"})
    public ResponseEntity<List<Insurance>> getUserInsurance(HttpServletRequest request, HttpServletResponse response) {

        final User currentUser = getCurrentLoggedInUser();

        List<Insurance> userInsuranceList = patientPortalService.getUserInsurance(currentUser);

        UserInformation userInformation = currentUser.getUserInformation();

        return new ResponseEntity<>(userInsuranceList, HttpStatus.OK);
    }

    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    public ResponseEntity<UserInformation> updateUserInformation(
            @RequestBody final UserInformation input, HttpServletRequest request, HttpServletResponse response) {

        final User currentUser = getCurrentLoggedInUser();

        currentUser.setFirstName(input.getUser().getFirstName());
        currentUser.setLastName(input.getUser().getLastName());
        currentUser.setMiddleName(input.getUser().getMiddleName());

        if(input.getId() == null || input.getId() == 0){
            input.setId(null);
        }

        input.setUser(currentUser);

        UserInformation userInformation = userService.saveUserInformation(input);
        currentUser.setUserInformation(userInformation);
        userService.updateUser(currentUser);

        if (userInformation == null) {
            //TODO throw error
            userInformation = new UserInformation();
        }

        currentUser.setUserInformation(null);
        userInformation.setUser(currentUser);

        return new ResponseEntity<>(userInformation, HttpStatus.OK);
    }

    @RequestMapping(value = {"/update_insurance"}, method = RequestMethod.POST)
    public ResponseEntity<Insurance> updateInsurance(
            @RequestBody final Insurance input, HttpServletRequest request, HttpServletResponse response) {

        final User currentUser = getCurrentLoggedInUser();

        if(input.getId() == null || input.getId() == 0){
            input.setId(null);
        }

        input.setUser(currentUser);

        Insurance insurance = patientPortalService.saveInsurance(input);
        Set<Insurance> userInsuranceList = currentUser.getInsurance();
        if (userInsuranceList == null) {
            userInsuranceList = new HashSet<>();
        }
        addInsuranceIfNotExists(insurance, userInsuranceList);

        currentUser.setInsurance(userInsuranceList);

        userService.updateUser(currentUser);


        return new ResponseEntity<>(insurance, HttpStatus.OK);
    }

    private void addInsuranceIfNotExists(Insurance insurance, Set<Insurance> userInsuranceList) {
        boolean found = false;
        for (Insurance t : userInsuranceList) {
            if (t.getId().equals(insurance.getId())) {
                found = true;
            }
        }

        if (!found){
            userInsuranceList.add(insurance);
        }
    }

    private User getCurrentLoggedInUser() {
        String userName = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            userName = ((UserDetails) principal).getUsername();
        } else {
            userName = principal.toString();
        }

        return userService.findByUsername(userName);
    }
}
