package edu.umuc.swen670.fall2017.pharmacy.service;

import edu.umuc.swen670.fall2017.pharmacy.dao.UserRepository;
import edu.umuc.swen670.fall2017.pharmacy.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AdminPortalServiceImpl implements AdminPortalService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public User approveAccount(Long userId) {
        return null;
    }
}
