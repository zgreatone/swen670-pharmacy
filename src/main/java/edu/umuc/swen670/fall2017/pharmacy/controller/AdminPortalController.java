package edu.umuc.swen670.fall2017.pharmacy.controller;

import edu.umuc.swen670.fall2017.pharmacy.model.User;
import edu.umuc.swen670.fall2017.pharmacy.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
@RequestMapping("/admin")
public class AdminPortalController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = {"", "/approve"}, method = RequestMethod.GET)
    public String getPage(HttpServletRequest request, HttpServletResponse response, Model model) {

        model.addAttribute("user", getCurrentLoggedInUser());
        return "account_approval";
    }

    @RequestMapping(value = {"/users"})
    public ResponseEntity<List<User>> getUserDetails(HttpServletRequest request, HttpServletResponse response) {

        final List<User> unApprovedUsers = userService.findAllUnApprovedUsers();

        return new ResponseEntity<>(unApprovedUsers, HttpStatus.OK);
    }

    @RequestMapping(value = {"/activate_user"}, method = RequestMethod.POST)
    public ResponseEntity<User> activateUser(
            @RequestBody final User input, HttpServletRequest request, HttpServletResponse response) {

        final User user = userService.findUserByEmail(input.getEmail());
        user.setEnabled(true);
        userService.updateUser(user);

        return new ResponseEntity<>(userService.findUserByEmail(input.getEmail()), HttpStatus.OK);
    }

    @RequestMapping(value = {"/delete_user"}, method = RequestMethod.DELETE)
    public ResponseEntity<User> deleteUser(
            @RequestBody final User input, HttpServletRequest request, HttpServletResponse response) {

        userService.deleteUser(input);

        return new ResponseEntity<>(input, HttpStatus.OK);
    }

    private User getCurrentLoggedInUser() {
        String userName = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            userName = ((UserDetails) principal).getUsername();
        } else {
            userName = principal.toString();
        }

        return userService.findByUsername(userName);
    }
}
