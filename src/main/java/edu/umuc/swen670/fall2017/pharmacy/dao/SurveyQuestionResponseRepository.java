package edu.umuc.swen670.fall2017.pharmacy.dao;

import edu.umuc.swen670.fall2017.pharmacy.model.SurveyQuestionResponse;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SurveyQuestionResponseRepository extends JpaRepository<SurveyQuestionResponse, Long> {
}
