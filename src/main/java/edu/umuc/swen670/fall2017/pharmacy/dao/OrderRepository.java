package edu.umuc.swen670.fall2017.pharmacy.dao;

import edu.umuc.swen670.fall2017.pharmacy.model.Drug;
import edu.umuc.swen670.fall2017.pharmacy.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Long> {
}
