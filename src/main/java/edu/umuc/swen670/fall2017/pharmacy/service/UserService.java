package edu.umuc.swen670.fall2017.pharmacy.service;


import edu.umuc.swen670.fall2017.pharmacy.model.Role;
import edu.umuc.swen670.fall2017.pharmacy.model.User;
import edu.umuc.swen670.fall2017.pharmacy.model.UserInformation;

import java.util.List;

public interface UserService {

    User findUserByEmail(String email);

    User findByUsername(String username);

    List<Role> findAllRoles();

    void saveUser(User user);

    void deleteUser(final User user);

    void updateUser(User user);

    List<User> findAllUsers();

    List<User> findAllUnApprovedUsers();

    UserInformation getUserInformation(final String userName);

    UserInformation saveUserInformation(final UserInformation userInformation);

}
