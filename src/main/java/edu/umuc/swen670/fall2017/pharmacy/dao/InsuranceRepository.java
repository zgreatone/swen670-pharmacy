package edu.umuc.swen670.fall2017.pharmacy.dao;

import edu.umuc.swen670.fall2017.pharmacy.model.Insurance;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InsuranceRepository extends JpaRepository<Insurance, Long> {
}
