package edu.umuc.swen670.fall2017.pharmacy.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Set;


@Entity
@Table(name = "application_user")
@JsonIgnoreProperties(value = {
        "roles",
        "prescriptions",
        "insurance",
        "fluShots",
        "orders",
        "password",
        "passwordConfirm"})
public class User {
    private Long id;
    private String username;
    private String middleName;
    private String firstName;


    private String lastName;

    private String email;
    private String password;
    private String passwordConfirm;

    private boolean enabled;
    private Set<Role> roles;

    private Set<Prescription> prescriptions;

    private UserInformation userInformation;

    private Set<Insurance> insurance;

    private Set<FluShotHistory> fluShots;

    private Set<Order> orders;


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Transient
    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
    public Set<Role> getRoles() {
        return roles;
    }

    //    @ManyToMany
//    @JoinTable(name = "user_flushot_history", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "flushot_history_id"))
    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
    public Set<FluShotHistory> getFluShots() {
        return fluShots;
    }

    @OneToMany(mappedBy = "user")
    public Set<Prescription> getPrescriptions() {
        return prescriptions;
    }

    public void setPrescriptions(Set<Prescription> prescriptions) {
        this.prescriptions = prescriptions;
    }

    public void setFluShots(Set<FluShotHistory> fluShots) {
        this.fluShots = fluShots;
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_information_id")
    public UserInformation getUserInformation() {
        return userInformation;
    }

    public void setUserInformation(UserInformation userInformation) {
        this.userInformation = userInformation;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }


    public void setInsurance(Set<Insurance> insurance) {
        this.insurance = insurance;
    }

    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
    public Set<Insurance> getInsurance() {
        return insurance;
    }

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
    public Set<Order> getOrders() {
        return orders;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public void setOrders(Set<Order> orders) {
        this.orders = orders;
    }
}