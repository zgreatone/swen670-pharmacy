package edu.umuc.swen670.fall2017.pharmacy.dao;

import edu.umuc.swen670.fall2017.pharmacy.model.Drug;
import edu.umuc.swen670.fall2017.pharmacy.model.FluShot;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FluShotRepository extends JpaRepository<FluShot, Long> {
}
