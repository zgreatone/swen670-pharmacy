package edu.umuc.swen670.fall2017.pharmacy.controller;

import edu.umuc.swen670.fall2017.pharmacy.model.Drug;
import edu.umuc.swen670.fall2017.pharmacy.model.Prescription;
import edu.umuc.swen670.fall2017.pharmacy.model.User;
import edu.umuc.swen670.fall2017.pharmacy.service.PharmacistPortalService;
import edu.umuc.swen670.fall2017.pharmacy.service.UserService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/pharmacist")
public class PharmacistPortalController {

    @Autowired
    private PharmacistPortalService pharmacistPortalService;

    @Autowired
    private UserService userService;

    @RequestMapping(value = {"", "/prescription"}, method = RequestMethod.GET)
    public String getPrescriptionPage(HttpServletRequest request, HttpServletResponse response, Model model) {

        return "prescription_admin";
    }

    @RequestMapping(value = {"", "/patient_record"}, method = RequestMethod.GET)
    public String getPatientRecordPage(HttpServletRequest request, HttpServletResponse response, Model model) {

        return "patient_record";
    }

    @RequestMapping(value = {"", "/drug_inventory"}, method = RequestMethod.GET)
    public String getDrugInventoryPage(HttpServletRequest request, HttpServletResponse response, Model model) {

        return "drug_inventory";
    }

    @RequestMapping(value = {"", "/drug_compounding"}, method = RequestMethod.GET)
    public String getDrugCompoundingPage(HttpServletRequest request, HttpServletResponse response, Model model) {

        return "drug_compounding";
    }

    @RequestMapping(value = {"", "/billing"}, method = RequestMethod.GET)
    public String getBillingPage(HttpServletRequest request, HttpServletResponse response, Model model) {

        return "billing";
    }

    @RequestMapping(value = {"/patients"})
    public ResponseEntity<Map<String, Object>> getPatientsDataTables(HttpServletRequest request, HttpServletResponse response) {

        Map<String, Object> returnMap = new HashMap<>();

        final List<User> patients = pharmacistPortalService.getAllPatients();

        returnMap.put("data", patients);


        return new ResponseEntity<>(returnMap, HttpStatus.OK);
    }

    @RequestMapping(value = {"/datatable_drugs"})
    public ResponseEntity<Map<String, Object>> getDrugsDataTables(HttpServletRequest request, HttpServletResponse response) {

        Map<String, Object> returnMap = new HashMap<>();

        final List<Drug> drugs = pharmacistPortalService.getAllDrugs();

        returnMap.put("data", drugs);


        return new ResponseEntity<>(returnMap, HttpStatus.OK);
    }

    @RequestMapping(value = {"/datatable_prescriptions"})
    public ResponseEntity<Map<String, Object>> getPrescriptionDataTables(HttpServletRequest request, HttpServletResponse response) {

        Map<String, Object> returnMap = new HashMap<>();

        final List<Prescription> prescriptions = pharmacistPortalService.getAllFilePrescription(getCurrentLoggedInUser());

        returnMap.put("data", prescriptions);


        return new ResponseEntity<>(returnMap, HttpStatus.OK);
    }

    @RequestMapping(value = {"/users"})
    public ResponseEntity<List<User>> getPatients(HttpServletRequest request, HttpServletResponse response) {

        final List<User> patients = pharmacistPortalService.getAllPatients();

        return new ResponseEntity<>(patients, HttpStatus.OK);
    }

    @RequestMapping(value = {"/drugs"})
    public ResponseEntity<List<Drug>> getDrugs(HttpServletRequest request, HttpServletResponse response) {

        final List<Drug> drugs = pharmacistPortalService.getAllDrugs();

        return new ResponseEntity<>(drugs, HttpStatus.OK);
    }

    @RequestMapping(value = {"/create"}, method = RequestMethod.POST)
    public ResponseEntity<Prescription> createPrescription(
            @RequestBody final String json, HttpServletRequest request, HttpServletResponse response) {

        JSONObject jsonObject = new JSONObject(json);

        final String userName = jsonObject.getJSONObject("patient").getString("username");
        final User patient = userService.findByUsername(userName);

        Long drugId = jsonObject.getJSONObject("drug").getLong("id");
        final Drug drug = pharmacistPortalService.getDrug(drugId);

        String start_dt = jsonObject.getString("startDate");
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate = null;
        try {
            startDate = formatter.parse(start_dt);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Prescription prescription = new Prescription();
        prescription.setDrug(drug);
        prescription.setUser(patient);
        prescription.setDoctorName(jsonObject.getString("doctorName"));
        prescription.setDoctorPhoneNumber(jsonObject.getString("doctorNumber"));
        prescription.setRefillCount(jsonObject.getInt("refillCount"));
        prescription.setDosage(jsonObject.getString("dosage"));
        prescription.setStartDate(startDate);
        prescription.setPharmacist(getCurrentLoggedInUser());

        prescription = pharmacistPortalService.save(prescription);

        return new ResponseEntity<>(prescription, HttpStatus.OK);
    }

    private User getCurrentLoggedInUser() {
        String userName = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            userName = ((UserDetails) principal).getUsername();
        } else {
            userName = principal.toString();
        }

        return userService.findByUsername(userName);
    }
}
