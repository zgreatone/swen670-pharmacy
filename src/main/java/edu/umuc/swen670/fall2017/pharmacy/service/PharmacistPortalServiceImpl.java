package edu.umuc.swen670.fall2017.pharmacy.service;

import edu.umuc.swen670.fall2017.pharmacy.dao.DrugRepository;
import edu.umuc.swen670.fall2017.pharmacy.dao.PrescriptionRepository;
import edu.umuc.swen670.fall2017.pharmacy.dao.UserRepository;
import edu.umuc.swen670.fall2017.pharmacy.model.Drug;
import edu.umuc.swen670.fall2017.pharmacy.model.Prescription;
import edu.umuc.swen670.fall2017.pharmacy.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PharmacistPortalServiceImpl implements PharmacistPortalService {

    @Autowired
    private DrugRepository drugRepository;

    @Autowired
    private PrescriptionRepository prescriptionRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<User> getAllPatients() {
        return userRepository.findAll()
                .stream()
                .filter(user -> hasPatientRole(user))
                .collect(Collectors.toList());
    }

    private boolean hasPatientRole(final User user) {
        final String patientRoleName = "PATIENT";

        boolean idExists = user.getRoles().stream()
                .anyMatch(role -> role.getName().equalsIgnoreCase(patientRoleName));

//        boolean idExists = user.getRoles().stream()
//                .map(Role::getName)
//                .anyMatch(patientRoleName::equalsIgnoreCase);

        return idExists;

//
//
//        for (Role role : user.getRoles()) {
//            if (role.getName().equalsIgnoreCase("PATIENT")) {
//                return true;
//            }
//        }
//        return false;
    }

    @Override
    public Drug getDrug(Long id) {
        return drugRepository.findOne(id);
    }

    @Override
    public Prescription save(Prescription prescription) {
        return prescriptionRepository.save(prescription);
    }

    @Override
    public List<Prescription> getAllFilePrescription(User pharmacist) {
        return prescriptionRepository.findAll()
                .stream()
                .filter(prescription -> prescription.getPharmacist().getUsername().equalsIgnoreCase(pharmacist.getUsername()))
                .collect(Collectors.toList());
    }

    @Override
    public List<Drug> getAllDrugs() {
        return drugRepository.findAll();
    }
}
