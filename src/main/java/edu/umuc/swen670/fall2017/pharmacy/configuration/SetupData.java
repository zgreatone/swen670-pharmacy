package edu.umuc.swen670.fall2017.pharmacy.configuration;

import edu.umuc.swen670.fall2017.pharmacy.dao.DrugCompanyRepository;
import edu.umuc.swen670.fall2017.pharmacy.dao.DrugRepository;
import edu.umuc.swen670.fall2017.pharmacy.dao.FluShotRepository;
import edu.umuc.swen670.fall2017.pharmacy.dao.RoleRepository;
import edu.umuc.swen670.fall2017.pharmacy.dao.SurveyQuestionRepository;
import edu.umuc.swen670.fall2017.pharmacy.dao.UserRepository;
import edu.umuc.swen670.fall2017.pharmacy.model.Drug;
import edu.umuc.swen670.fall2017.pharmacy.model.DrugCompany;
import edu.umuc.swen670.fall2017.pharmacy.model.FluShot;
import edu.umuc.swen670.fall2017.pharmacy.model.Role;
import edu.umuc.swen670.fall2017.pharmacy.model.SurveyQuestion;
import edu.umuc.swen670.fall2017.pharmacy.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;

@Component
public class SetupData {
    private static final String PHARMACIST = "PHARMACIST";
    private static final String PATIENT = "PATIENT";
    private static final String ADMIN = "ADMIN";
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private DrugRepository drugRepository;

    @Autowired
    private DrugCompanyRepository drugCompanyRepository;

    @Autowired
    private SurveyQuestionRepository surveyQuestionRepository;


    @Autowired
    private FluShotRepository fluShotRepository;


    @PostConstruct
    public void init() {
        initRolesAndPrivileges();
        initUsers();
        initSampleData();
        initFluShot();
        initSurveyData();
    }

    private void initFluShot() {
        FluShot fluShot = new FluShot();

        fluShot.setStrain("Fluzone Quad");
        fluShot.setActiveDate(new Date());

        fluShotRepository.saveAndFlush(fluShot);
    }

    private void initUsers() {
        final Role role1 = roleRepository.findByName(PHARMACIST);
        final Role role2 = roleRepository.findByName(PATIENT);
        final Role role3 = roleRepository.findByName(ADMIN);
        //
        final User user1 = new User();
        user1.setEmail("pharmacist@foo.bar");
        user1.setPassword(encoder.encode("password"));
        user1.setRoles(new HashSet<Role>(Arrays.asList(role1)));
        user1.setEnabled(true);
        user1.setFirstName("John");
        user1.setUsername("JohnFoo");
        userRepository.save(user1);
        //
        final User user2 = new User();
        user2.setEmail("patient@foo.bar");
        user2.setPassword(encoder.encode("password"));
        user2.setRoles(new HashSet<Role>(Arrays.asList(role2)));
        user2.setEnabled(true);
        user2.setFirstName("Tom");
        user2.setLastName("Price");
        user2.setUsername("TomFoo");
        userRepository.save(user2);

        final User user3 = new User();
        user3.setEmail("admin@foo.bar");
        user3.setPassword(encoder.encode("password"));
        user3.setRoles(new HashSet<Role>(Arrays.asList(role3)));
        user3.setEnabled(true);
        user3.setFirstName("Administrator");
        user3.setUsername("admin");
        userRepository.save(user3);
    }

    private void initRolesAndPrivileges() {

        final Role role1 = new Role(PATIENT);
        roleRepository.save(role1);

        final Role role2 = new Role(PHARMACIST);
        roleRepository.save(role2);

        final Role role3 = new Role(ADMIN);
        roleRepository.save(role3);
    }

    private void initSampleData() {

        DrugCompany drugCompany = new DrugCompany();
        drugCompany.setAddress("This is My Address");
        drugCompany.setName("Pfizer");
        drugCompany.setPhoneNumber("555-555-5555");

        drugCompany = drugCompanyRepository.saveAndFlush(drugCompany);

        Drug drug = new Drug();
        drug.setCompany(drugCompany);
        drug.setName("Pennicilin");
        drug.setQuantity(300l);
        drug.setRetailPrice(11.93);
        drug.setWholeSalePrice(6.35);

        drug = drugRepository.saveAndFlush(drug);

        drug = new Drug();
        drug.setCompany(drugCompany);
        drug.setName("Panadol");
        drug.setQuantity(300l);
        drug.setRetailPrice(11.93);
        drug.setWholeSalePrice(6.35);

        drug = drugRepository.saveAndFlush(drug);

    }

    private void initSurveyData() {

        SurveyQuestion surveyQuestion = new SurveyQuestion();
        surveyQuestion.setQuestion("You Sick?");

        surveyQuestionRepository.saveAndFlush(surveyQuestion);

        surveyQuestion = new SurveyQuestion();
        surveyQuestion.setQuestion("History of Diabetes?");

        surveyQuestionRepository.saveAndFlush(surveyQuestion);
    }

}
