package edu.umuc.swen670.fall2017.pharmacy.service;

import edu.umuc.swen670.fall2017.pharmacy.model.FluShot;
import edu.umuc.swen670.fall2017.pharmacy.model.FluShotHistory;
import edu.umuc.swen670.fall2017.pharmacy.model.Insurance;
import edu.umuc.swen670.fall2017.pharmacy.model.Prescription;
import edu.umuc.swen670.fall2017.pharmacy.model.SurveyQuestion;
import edu.umuc.swen670.fall2017.pharmacy.model.SurveyQuestionResponse;
import edu.umuc.swen670.fall2017.pharmacy.model.SurveyResponse;
import edu.umuc.swen670.fall2017.pharmacy.model.User;

import java.util.List;

public interface PatientPortalService {

    List<SurveyQuestionResponse> getHealthSurveyQuestionResponse();

    List<Prescription> getUserPrescriptions(User user);

    List<Insurance> getUserInsurance(User user);

    Insurance saveInsurance(Insurance insurance);

    List<SurveyResponse> getSurveyResponse(User user);

    SurveyResponse saveSurveyResponse(SurveyResponse surveyResponse);

    List<FluShotHistory> getFlushotHistory(User user);

    List<FluShot> getFlushot();

    FluShotHistory saveFlushotHistory(FluShotHistory fluShotHistory);
}
