package edu.umuc.swen670.fall2017.pharmacy.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "user_insurance")
@JsonIgnoreProperties(value = {"user"})
public class Insurance {

    private Long id;
    private User user;
    private String companyName;
    private Double coPay;
    private String primaryHolderName;
    private String insurancePhoneNumber;
    private String insuranceGroupNumber;
    private String billingStreet;
    private String billingState;
    private String billingCity;
    private String billingZip;


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Double getCoPay() {
        return coPay;
    }

    public void setCoPay(Double coPay) {
        this.coPay = coPay;
    }

    public String getPrimaryHolderName() {
        return primaryHolderName;
    }

    public void setPrimaryHolderName(String primaryHolderName) {
        this.primaryHolderName = primaryHolderName;
    }

    public String getInsurancePhoneNumber() {
        return insurancePhoneNumber;
    }

    public void setInsurancePhoneNumber(String insurancePhoneNumber) {
        this.insurancePhoneNumber = insurancePhoneNumber;
    }

    public String getInsuranceGroupNumber() {
        return insuranceGroupNumber;
    }

    public void setInsuranceGroupNumber(String insuranceGroupNumber) {
        this.insuranceGroupNumber = insuranceGroupNumber;
    }

    public String getBillingStreet() {
        return billingStreet;
    }

    public void setBillingStreet(String billingStreet) {
        this.billingStreet = billingStreet;
    }

    public String getBillingState() {
        return billingState;
    }

    public void setBillingState(String billingState) {
        this.billingState = billingState;
    }

    public String getBillingCity() {
        return billingCity;
    }

    public void setBillingCity(String billingCity) {
        this.billingCity = billingCity;
    }

    public String getBillingZip() {
        return billingZip;
    }

    public void setBillingZip(String billingZip) {
        this.billingZip = billingZip;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
