package edu.umuc.swen670.fall2017.pharmacy.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "flushot")
@JsonIgnoreProperties(value = {"fluShotHistory"})
public class FluShot {

    private Long id;
    private String strain;
    private String visibleName;

    @JsonFormat(pattern="yyyy-MM-dd")
    private Date activeDate;

    private Set<FluShotHistory> fluShotHistory;


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStrain() {
        return strain;
    }

    public void setStrain(String strain) {
        this.strain = strain;
    }

    public Date getActiveDate() {
        return activeDate;
    }

    public void setActiveDate(Date activeDate) {
        this.activeDate = activeDate;
    }

    public String getVisibleName() {
        return visibleName;
    }

    public void setVisibleName(String visibleName) {
        this.visibleName = visibleName;
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fluShot")
    public Set<FluShotHistory> getFluShotHistory() {
        return fluShotHistory;
    }

    public void setFluShotHistory(Set<FluShotHistory> fluShotHistory) {
        this.fluShotHistory = fluShotHistory;
    }

}
