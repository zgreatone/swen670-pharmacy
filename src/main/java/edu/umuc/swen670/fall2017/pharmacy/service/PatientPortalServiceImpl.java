package edu.umuc.swen670.fall2017.pharmacy.service;

import edu.umuc.swen670.fall2017.pharmacy.dao.FluShotHistoryRepository;
import edu.umuc.swen670.fall2017.pharmacy.dao.FluShotRepository;
import edu.umuc.swen670.fall2017.pharmacy.dao.InsuranceRepository;
import edu.umuc.swen670.fall2017.pharmacy.dao.OrderRepository;
import edu.umuc.swen670.fall2017.pharmacy.dao.PrescriptionRepository;
import edu.umuc.swen670.fall2017.pharmacy.dao.SurveyResponseRepository;
import edu.umuc.swen670.fall2017.pharmacy.model.FluShot;
import edu.umuc.swen670.fall2017.pharmacy.model.FluShotHistory;
import edu.umuc.swen670.fall2017.pharmacy.model.Insurance;
import edu.umuc.swen670.fall2017.pharmacy.model.Prescription;
import edu.umuc.swen670.fall2017.pharmacy.model.SurveyQuestionResponse;
import edu.umuc.swen670.fall2017.pharmacy.model.SurveyResponse;
import edu.umuc.swen670.fall2017.pharmacy.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class PatientPortalServiceImpl implements PatientPortalService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private PrescriptionRepository prescriptionRepository;

    @Autowired
    private InsuranceRepository insuranceRepository;

    @Autowired
    private SurveyResponseRepository surveyResponseRepository;

    @Autowired
    private FluShotRepository fluShotRepository;

    @Autowired
    private FluShotHistoryRepository fluShotHistoryRepository;

    @Override
    public List<Insurance> getUserInsurance(User user) {
        return insuranceRepository.findAll().stream()
                .filter(prescription -> prescription.getUser().getUsername().equalsIgnoreCase(user.getUsername()))
                .collect(Collectors.toList());
    }

    @Override
    public List<SurveyResponse> getSurveyResponse(User user) {
        return surveyResponseRepository.findAll().stream()
                .filter(prescription -> prescription.getUser().getUsername().equalsIgnoreCase(user.getUsername()))
                .collect(Collectors.toList());
    }

    @Override
    public FluShotHistory saveFlushotHistory(FluShotHistory fluShotHistory) {
        return fluShotHistoryRepository.saveAndFlush(fluShotHistory);
    }

    @Override
    public List<FluShotHistory> getFlushotHistory(User user) {
        return fluShotHistoryRepository.findAll().stream()
                .filter(prescription -> prescription.getUser().getUsername().equalsIgnoreCase(user.getUsername()))
                .collect(Collectors.toList());
    }

    @Override
    public List<FluShot> getFlushot() {
        return fluShotRepository.findAll();
    }

    @Override
    public SurveyResponse saveSurveyResponse(final SurveyResponse surveyResponse) {
        return surveyResponseRepository.save(surveyResponse);
    }

    @Override
    public Insurance saveInsurance(Insurance insurance) {
        return insuranceRepository.save(insurance);
    }

    @Override
    public List<Prescription> getUserPrescriptions(User user) {
        return prescriptionRepository.findAll().stream()
                .filter(prescription -> prescription.getUser().getUsername().equalsIgnoreCase(user.getUsername()))
                .collect(Collectors.toList());
    }

    @Override
    public List<SurveyQuestionResponse> getHealthSurveyQuestionResponse() {
        return null;
    }
}
