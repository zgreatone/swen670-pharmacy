package edu.umuc.swen670.fall2017.pharmacy.service;

import edu.umuc.swen670.fall2017.pharmacy.model.User;

public interface AdminPortalService {

    User approveAccount(final Long userId);
}
