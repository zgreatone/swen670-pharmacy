package edu.umuc.swen670.fall2017.pharmacy.dao;

import edu.umuc.swen670.fall2017.pharmacy.model.FluShot;
import edu.umuc.swen670.fall2017.pharmacy.model.FluShotHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FluShotHistoryRepository extends JpaRepository<FluShotHistory, Long> {
}
