package edu.umuc.swen670.fall2017.pharmacy.dao;

import edu.umuc.swen670.fall2017.pharmacy.model.DrugCompany;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DrugCompanyRepository extends JpaRepository<DrugCompany, Long> {
}
