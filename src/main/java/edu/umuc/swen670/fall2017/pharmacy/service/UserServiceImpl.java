package edu.umuc.swen670.fall2017.pharmacy.service;


import edu.umuc.swen670.fall2017.pharmacy.dao.RoleRepository;
import edu.umuc.swen670.fall2017.pharmacy.dao.UserInformationRepository;
import edu.umuc.swen670.fall2017.pharmacy.dao.UserRepository;
import edu.umuc.swen670.fall2017.pharmacy.model.Role;
import edu.umuc.swen670.fall2017.pharmacy.model.User;
import edu.umuc.swen670.fall2017.pharmacy.model.UserInformation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserInformationRepository userInformationRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public User findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }


    public void saveUser(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        HashSet<Role> roles = new HashSet<Role>();
        for (final Role role : user.getRoles()) {
            final Role foundRole = roleRepository.findByName(role.getName());
            if (foundRole != null) {
                roles.add(foundRole);

                if (foundRole.getName().equalsIgnoreCase("PHARMACIST")) {
                    user.setEnabled(false);
                } else {
                    user.setEnabled(true);
                }
            }
        }
        user.setRoles(roles);
        userRepository.save(user);
    }

    @Override
    public void deleteUser(User user) {
        userRepository.delete(user.getId());
    }

    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    public List<Role> findAllRoles() {
        return roleRepository.findAll()
                .stream()
                .filter(role -> !role.getName()
                        .equalsIgnoreCase("ADMIN"))
                .collect(Collectors.toList());
    }

    @Override
    public List<User> findAllUnApprovedUsers() {
        return userRepository.findAll()
                .stream()
                .filter(user -> !user.isEnabled())
                .collect(Collectors.toList());
    }

    public void updateUser(User user) {
        userRepository.save(user);
    }

    public List<User> findAllUsers() {
        return userRepository.findAll();
    }


    public UserInformation getUserInformation(final String userName) {
        final User user = findByUsername(userName);
        return user.getUserInformation();
    }

    public UserInformation saveUserInformation(final UserInformation userInformation) {

        return userInformationRepository.save(userInformation);
    }
}