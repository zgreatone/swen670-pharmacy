package edu.umuc.swen670.fall2017.pharmacy.controller;

import edu.umuc.swen670.fall2017.pharmacy.model.FluShotHistory;
import edu.umuc.swen670.fall2017.pharmacy.model.Prescription;
import edu.umuc.swen670.fall2017.pharmacy.model.SurveyResponse;
import edu.umuc.swen670.fall2017.pharmacy.model.User;
import edu.umuc.swen670.fall2017.pharmacy.service.PatientPortalService;
import edu.umuc.swen670.fall2017.pharmacy.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Controller
@RequestMapping("/patient")
public class PatientPortalController {

    @Autowired
    private UserService userService;

    @Autowired
    private PatientPortalService patientPortalService;

    @RequestMapping(value = {"", "/prescription"}, method = RequestMethod.GET)
    public String getPrescriptionPage(HttpServletRequest request, HttpServletResponse response, Model model) {

        return "prescription";
    }

    @RequestMapping(value = "/flushot", method = RequestMethod.GET)
    public String getFlushotPage(HttpServletRequest request, HttpServletResponse response, Model model) {

        return "flushot";
    }

    @RequestMapping(value = "/survey", method = RequestMethod.GET)
    public String getSurveyPage(HttpServletRequest request, HttpServletResponse response, Model model) {

        return "survey";
    }

    @RequestMapping(value = {"/surveys"}, method = RequestMethod.GET)
    public ResponseEntity<SurveyResponse> getSurveyResponse(HttpServletRequest request, HttpServletResponse response) {

        final User currentUser = getCurrentLoggedInUser();

        SurveyResponse surveyResponse = new SurveyResponse();
        List<SurveyResponse> allResponses = patientPortalService.getSurveyResponse(currentUser);
        if (!allResponses.isEmpty()) {
            surveyResponse = allResponses.get(0);
        }

        return new ResponseEntity<>(surveyResponse, HttpStatus.OK);

    }

    @RequestMapping(value = {"/update_survey"}, method = RequestMethod.POST)
    public ResponseEntity<SurveyResponse> saveSurvey(
            @RequestBody final SurveyResponse input, HttpServletRequest request, HttpServletResponse response) {

        final User currentUser = getCurrentLoggedInUser();

        if (input.getId() == null || input.getId() == 0) {
            input.setId(null);
        }

        input.setUser(currentUser);

        SurveyResponse surveyResponse = patientPortalService.saveSurveyResponse(input);

        return new ResponseEntity<>(surveyResponse, HttpStatus.OK);

    }

    @RequestMapping(value = {"/update_flushot"}, method = RequestMethod.POST)
    public ResponseEntity<FluShotHistory> saveFlushot(
            @RequestBody final FluShotHistory input, HttpServletRequest request, HttpServletResponse response) {

        final User currentUser = getCurrentLoggedInUser();

        if (input.getId() == null || input.getId() == 0) {
            input.setId(null);
        }

        input.setUser(currentUser);

        input.setUser(currentUser);

        FluShotHistory fluShotHistory = patientPortalService.saveFlushotHistory(input);
        Set<FluShotHistory> fluShotHistorySet = currentUser.getFluShots();
        if (fluShotHistorySet == null) {
            fluShotHistorySet = new HashSet<>();
        }
        addInsuranceIfNotExists(fluShotHistory, fluShotHistorySet);

        currentUser.setFluShots(fluShotHistorySet);

        userService.updateUser(currentUser);


        return new ResponseEntity<>(fluShotHistory, HttpStatus.OK);

    }

    private void addInsuranceIfNotExists(FluShotHistory fluShotHistory, Set<FluShotHistory> fluShotHistorySet) {
        boolean found = false;
        for (FluShotHistory t : fluShotHistorySet) {
            if (t.getId().equals(fluShotHistory.getId())) {
                found = true;
            }
        }

        if (!found) {
            fluShotHistorySet.add(fluShotHistory);
        }
    }

    @RequestMapping(value = "/fluShots", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> getFlusShots(HttpServletRequest request, HttpServletResponse response, Model model) {

        final User currentUser = getCurrentLoggedInUser();

        Map<String, Object> responseObject = new HashMap<>();

        responseObject.put("fluShots", patientPortalService.getFlushot());

        List<FluShotHistory> allFlushots = patientPortalService.getFlushotHistory(currentUser);

        FluShotHistory latest = new FluShotHistory();

        if (!allFlushots.isEmpty()) {
            latest = allFlushots.get(0);
        }

        responseObject.put("myFluShots", allFlushots);
        responseObject.put("latest", latest);


        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @RequestMapping(value = {"/prescriptions"})
    public ResponseEntity<List<Prescription>> getPatients(HttpServletRequest request, HttpServletResponse response) {

        final List<Prescription> prescriptions = patientPortalService.getUserPrescriptions(getCurrentLoggedInUser());

        return new ResponseEntity<>(prescriptions, HttpStatus.OK);
    }

    private User getCurrentLoggedInUser() {
        String userName = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            userName = ((UserDetails) principal).getUsername();
        } else {
            userName = principal.toString();
        }

        return userService.findByUsername(userName);
    }
}
