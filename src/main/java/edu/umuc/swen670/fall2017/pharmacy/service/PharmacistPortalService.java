package edu.umuc.swen670.fall2017.pharmacy.service;

import edu.umuc.swen670.fall2017.pharmacy.model.Drug;
import edu.umuc.swen670.fall2017.pharmacy.model.Prescription;
import edu.umuc.swen670.fall2017.pharmacy.model.User;

import java.util.List;

public interface PharmacistPortalService {

    List<User> getAllPatients();

    List<Drug> getAllDrugs();

    Drug getDrug(Long id);

    Prescription save(Prescription prescription);

    List<Prescription> getAllFilePrescription(final User pharmacist);
}
